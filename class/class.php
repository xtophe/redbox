<?php 
class myfunction
{
	public static function return_month_fr($m)
	{
	 // par defaut on affiche la date du jour
	 	$tabmois=array('0', 'Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin','Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre','D�cembre');
	 // construction de la date formatee
	  	$monthfr = $tabmois[$m];
	 // affichage (remplacer 'echo' par 'return' pour retourner le resultat)
	 	return $monthfr;
	}
	public static function return_date_fr($timestamp)
	{
	 // par defaut on affiche la date du jour
	 	$jsem = date('w', strtotime($timestamp)); // jour de la semaine
	 	$jmois = date('j', strtotime($timestamp)); // jour du mois ('d' est aussi utilisable)
	 	$mois = date('n', strtotime($timestamp)); // mois de l'annee
	 	$annee = date('Y', strtotime($timestamp)); // l'annee
	 	$tabjour=array('dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi','samedi');
	 	$tabmois=array('0', 'janvier', 'f�vrier', 'mars', 'avril', 'mai', 'juin','juillet', 'ao�t', 'septembre', 'octobre', 'novembre','d�cembre');
	 // construction de la date formatee
	  	$datefr = $tabjour[$jsem]." $jmois ".$tabmois[$mois]." $annee";
	 // affichage (remplacer 'echo' par 'return' pour retourner le resultat)
	 	return $datefr;
	}
	public static function return_date_month_year_fr($timestamp)
	{
	 // par defaut on affiche la date du jour
	 	$jour = date('d', strtotime($timestamp)); // jour de la semaine
		$annee = date('Y', strtotime($timestamp)); // l'annee
	 	$mois = date('n', strtotime($timestamp)); // mois de l'annee
	 	$tabmois=array('0', 'Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin','Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre','D�cembre');
	 // construction de la date formatee
	  	$date = $jour." ".$tabmois[$mois]." $annee";
	 // affichage (remplacer 'echo' par 'return' pour retourner le resultat)
	 	return $date;
	}
	public static function return_date_en($timestamp)
	{
	 // par defaut on affiche la date du jour
	 	$jsem = date('w', strtotime($timestamp)); // jour de la semaine
	 	$jmois = date('j', strtotime($timestamp)); // jour du mois ('d' est aussi utilisable)
	 	$mois = date('n', strtotime($timestamp)); // mois de l'annee
	 	$annee = date('Y', strtotime($timestamp)); // l'annee
	 	$tabjour=array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday');
	 	$tabmois=array('0', 'January', 'February', 'March', 'April', 'Mai', 'June','July', 'August', 'September', 'October', 'November','December');
	 // construction de la date formatee
	  	$date_en = $tabjour[$jsem]." $jmois ".$tabmois[$mois]." $annee";
	 // affichage (remplacer 'echo' par 'return' pour retourner le resultat)
	 	return $date_en;
	}
	public static function input_date($mydate)
	{
		if ($mydate != "")
		{
			$date_tok = strtok($mydate, "/");
			$count = 0;
			while ($date_tok)
			{
				$count++;
				if($count==1)
				{
					$dd = $date_tok;
				}
				elseif($count==2)
				{
					$mm = $date_tok;
				}
				else
				{
					$yy = $date_tok;
				}
				$date_tok = strtok("/");
			}
			$mydate = $yy."-".$mm."-".$dd;
			return $mydate;
		}
	}
	public static function output_date($mydate)
	{
		if ($mydate != "")
		{
			$date_is = explode('-',$mydate);
			$yy = $date_is['0'];
			$mm = $date_is['1'];
			$check_dd = explode(' ',$date_is['2']);
			$dd =  $check_dd['0'];
			return $dd."/".$mm."/".$yy;
		}
	}
	public static function input_date_fr($mydate)
	{
		$mydate_explode=explode(" ",$mydate);
		$date=$mydate_explode[0];
		$date_explode=explode("-",$date);
		$yy=$date_explode[0];
		$mm=$date_explode[1];
		$jj=$date_explode[2];
		$time=$mydate_explode[1];
		$mydate=$jj."-".$mm."-".$yy;
		return $mydate;
	}
	public static function return_timestamp()
	{
		$yy = date("Y");
		$mm = date("m");
		$dd = date("d");
		$hour = date("G");
		$min = date("i");
		$sec = date("s");
		
		$mytime = $yy."-".$mm."-".$dd." ".$hour.":".$min.":".$sec;
		return $mytime;
	}
	
	//Truncate text after number of words						
	public static function truncate_text($phrase, $max_words)
	{
	   $phrase_array = explode(' ',$phrase);
	   
	   if(count($phrase_array) > $max_words && $max_words > 0)
	      $phrase = implode(' ',array_slice($phrase_array, 0, $max_words));
		  
	   return $phrase;
	}
	 public static function highlightWords($string,$words)
	 {
	    $string = str_ireplace($words, '<span style="background-color: yellow;">'.$words.'</span>', $string);
	    return $string;
	 }
	 /*Christophe 23/10/10*/
	public static function filter_variables($string)
	{
		$s_quote="'"; // not include in variable symbole
		$symbole="";
		$count_symbole="";
		$check_symbol="";
		$symbole = array('!','#','%','^','&','*','|','(',')','=','+','/','*','?','<','>','`',',','\\','"',';',':','~','�',$s_quote,'�','.');
		$count_symbole=count($symbole);
		$string=strip_tags($string);
		for($i=0;$i<=$count_symbole;$i++)
		{
			if(@strstr($string,$symbole[$i]))
			{
				$check_symbol="true";
			}
			if($check_symbol=="true")
			{
				$new_string=str_replace($symbole,'',$string);
			}
			else
			{
				$new_string=$string;
			}
		}
		$filter_string=$new_string;
						   //0   1   2   3   4   5   6   7   8  9
		$list_symbole=array('�','�','�','�','�','�','�','�','�','�');
		$count_list_symbole=count($list_symbole);
	
		if(strstr($filter_string,$list_symbole[0]))
		{
			$filter_string=str_replace($list_symbole[0],'a',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[1]))
		{
			$filter_string=str_replace($list_symbole[1],'a',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[2]))
		{
			$filter_string=str_replace($list_symbole[2],'e',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[3]))
		{
			$filter_string=str_replace($list_symbole[3],'e',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[4]))
		{
			$filter_string=str_replace($list_symbole[4],'e',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[5]))
		{
			$filter_string=str_replace($list_symbole[5],'i',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[6]))
		{
			$filter_string=str_replace($list_symbole[6],'o',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[7]))
		{
			$filter_string=str_replace($list_symbole[7],'u',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[8]))
		{
			$filter_string=str_replace($list_symbole[8],'c',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[9]))
		{
			$filter_string=str_replace($list_symbole[9],'u',$filter_string);
		}
		return strtolower($filter_string);
	}
	/*End Function remove symbol*/
	 /*Christophe 10/11/10*/
	public static function filter_meta_variables($string)
	{
		$s_quote="'"; // not include in variable symbole
		$symbole="";
		$count_symbole="";
		$check_symbol="";
		$symbole = array('!','#','%','^','&','*','|','(',')','=','+','/','*','?','<','>','`',',','\\','"',';',':','~','�',$s_quote);
		$count_symbole=count($symbole);
		$string=strip_tags($string);
		for($i=0;$i<=$count_symbole;$i++)
		{
			if(@strstr($string,$symbole[$i]))
			{
				$check_symbol="true";
			}
			if($check_symbol=="true")
			{
				$new_string=str_replace($symbole,'',$string);
			}
			else
			{
				$new_string=$string;
			}
		}
		$filter_string=$new_string;
						   //0   1   2   3   4   5   6   7   8  9
		$list_symbole=array('�','�','�','�','�','�','�','�','�','�');
		$count_list_symbole=count($list_symbole);
	
		if(strstr($filter_string,$list_symbole[0]))
		{
			$filter_string=str_replace($list_symbole[0],'a',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[1]))
		{
			$filter_string=str_replace($list_symbole[1],'a',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[2]))
		{
			$filter_string=str_replace($list_symbole[2],'e',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[3]))
		{
			$filter_string=str_replace($list_symbole[3],'e',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[4]))
		{
			$filter_string=str_replace($list_symbole[4],'e',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[5]))
		{
			$filter_string=str_replace($list_symbole[5],'i',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[6]))
		{
			$filter_string=str_replace($list_symbole[6],'o',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[7]))
		{
			$filter_string=str_replace($list_symbole[7],'u',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[8]))
		{
			$filter_string=str_replace($list_symbole[8],'c',$filter_string);
		}
		if(strstr($filter_string,$list_symbole[9]))
		{
			$filter_string=str_replace($list_symbole[9],'u',$filter_string);
		}
		return $filter_string;
	}
	/*End Function remove meta symbol*/
	public static function check_footer()
	{
		$rquery = mysql_query("select nom_footer from footer where lien_footer!=''");
		$count_footer=mysql_num_rows($rquery);
		$limit_footer=$count_footer+1;
		
		return $limit_footer;
	}
	/*Start of filter accent*/
	public static function filter_accent($accent_string)
	{
		$fount_accent="false";
						   //0   1   2   3   4   5   6   7   8   9    10          11        12         13         14        15        16         17        18         19
		$list_symbole=array('�','�','�','�','�','�','�','�','�','�','&agrave;','&acirc;','&eacute;','&egrave;','&ecirc;','&icirc;','&ocirc;','&ucirc;','&ccedil;','&ugrave;');
		//echo "wawa -1";
		$count_list_symbole=count($list_symbole);
		if(strstr($accent_string,$list_symbole[0]))
		{
			$accent_string=str_replace($list_symbole[0],'a',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[1]))
		{
			$accent_string=str_replace($list_symbole[1],'a',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[2]))
		{
			$accent_string=str_replace($list_symbole[2],'e',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[3]))
		{
			$accent_string=str_replace($list_symbole[3],'e',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[4]))
		{
			$accent_string=str_replace($list_symbole[4],'e',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[5]))
		{
			$accent_string=str_replace($list_symbole[5],'i',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[6]))
		{
			$accent_string=str_replace($list_symbole[6],'i',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[7]))
		{
			$accent_string=str_replace($list_symbole[7],'u',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[8]))
		{
			$accent_string=str_replace($list_symbole[8],'c',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[9]))
		{
			$accent_string=str_replace($list_symbole[9],'u',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[10]))
		{
			$accent_string=str_replace($list_symbole[10],'a',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[11]))
		{
			$accent_string=str_replace($list_symbole[11],'a',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[12]))
		{
			$accent_string=str_replace($list_symbole[12],'e',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[13]))
		{
			$accent_string=str_replace($list_symbole[13],'e',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[14]))
		{
			$accent_string=str_replace($list_symbole[14],'e',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[15]))
		{
			$accent_string=str_replace($list_symbole[15],'i',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[16]))
		{
			$accent_string=str_replace($list_symbole[16],'i',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[17]))
		{
			$accent_string=str_replace($list_symbole[17],'u',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[18]))
		{
			$accent_string=str_replace($list_symbole[18],'c',$accent_string);
			$fount_accent="true";
		}
		if(strstr($accent_string,$list_symbole[19]))
		{
			$accent_string=str_replace($list_symbole[19],'u',$accent_string);
			$fount_accent="true";
		}
		if($fount_accent=="false")
		{
			$accent_string=$accent_string;
		}
		return $accent_string;
	}
	/*End of filter accent*/
	public static function error_recurence($id_page) // check errors and status of po
	{
		$rquery = mysql_query("select * from page where id_page=$id_page");  
		$fields = mysql_num_fields($rquery); 
			while ($findlangue1 = mysql_fetch_array($rquery, MYSQL_ASSOC))
			{
				for ($i=0; $i < $fields; $i++  )  //fields
				{ 
					$fieldName = mysql_field_name($rquery, $i); 
					$field_label = "fieldName";
					$$fieldName = $findlangue1[$fieldName];
				}
			}
		//$count_meta_title=substr_count(myfunction::filter_accent(strtolower(strip_tags($meta_title))),strtolower($recurence_mot));
		//$count_meta_keywords=substr_count(myfunction::filter_accent($meta_keywords),strtolower($recurence_mot));
		$count_meta_description=substr_count(myfunction::filter_accent(strtolower($meta_description)),strtolower($recurence_mot));
	    //$count_titre_pages=substr_count(myfunction::filter_accent(strtolower(strip_tags($titre_page))),strtolower($recurence_mot));
		$count_texte_page=substr_count(myfunction::filter_accent(strtolower(strip_tags($texte_page))),strtolower($recurence_mot));
		
		/*if($count_meta_title > 0 )
		{
			$rec_meta_title="good";
		} 
		else
		{
			$rec_meta_title="bad";
		}*/
		if($count_meta_description > 1 )
		{
			$rec_meta_description="good";
		} 
		else
		{
			$rec_meta_description="bad";
		}
		/*if($count_meta_keywords > 0 )
		{
			$rec_meta_keywords="good";
		} 
		else
		{
			$rec_meta_keywords="bad";
		}*/
		/*if($count_titre_pages > 0 )
		{
			$rec_titre_page="good";
		} 
		else
		{
			$rec_titre_page="bad";
		}*/
		if($count_texte_page > 5 )
		{
			$rec_texte_page="good";
		} 
		else
		{
			$rec_texte_page="bad";
		}
		if($id_footer==0)
		{
			$footer_present="no";
		}
		else
		{
			$footer_present="yes";
		}
		//if( ($rec_meta_title=="good") && ($rec_meta_description=="good") && ($rec_meta_keywords=="good") && ($rec_titre_page=="good") && ($rec_texte_page=="good") && ($footer_present=="yes") )
		if( ($rec_meta_description=="good") && ($rec_texte_page=="good") && ($footer_present=="yes") )
		{
			$status_error="ok";
		}
		else
		{
			$status_error="error";
		}
		return $status_error;
	}
	public static function error_footer($id_page) // check errors and status of po
	{
		$rquery = mysql_query("select * from page where id_page=$id_page");  
		$fields = mysql_num_fields($rquery); 
			while ($findlangue1 = mysql_fetch_array($rquery, MYSQL_ASSOC))
			{
				for ($i=0; $i < $fields; $i++  )  //fields
				{ 
					$fieldName = mysql_field_name($rquery, $i); 
					$field_label = "fieldName";
					$$fieldName = $findlangue1[$fieldName];
				}
			}
		$count_meta_description=substr_count(myfunction::filter_accent(strtolower($meta_description)),strtolower($recurence_mot));
		$count_texte_page=substr_count(myfunction::filter_accent(strtolower(strip_tags($texte_page))),strtolower($recurence_mot));
		
		
		if($count_meta_description > 1 )
		{
			$rec_meta_description="good";
		} 
		else
		{
			$rec_meta_description="bad";
		}
		if($count_texte_page > 5 )
		{
			$rec_texte_page="good";
		} 
		else
		{
			$rec_texte_page="bad";
		}
		if( ($rec_meta_description=="good") && ($rec_texte_page=="good") && ($id_footer==0) )
		{
			$status_footer="no_footer";
		}
		else
		{
			$status_footer="all_good";
		}
		return $status_footer;
	}
	
	public static function time_today()
	{
		$yy = date("Y");
		$mm = date("m");
		$dd = date("d");
		
		$mytime = $yy."-".$mm."-".$dd;
		return $mytime;
	}
	
	public static function upload_file($fieldname,$path,$new_name)
	{
		if(is_dir($path))
		{			
			copy($_FILES[$fieldname]['tmp_name'], $path."/" .$new_name) 
			or die("Couldn't copy the file!"); 
		}
		else
		{
			print "Path is invalid normal...";
		}	
		
	}
	
	public static function image_upload_resized($fieldname,$path,$new_file_name,$nw)
	{	
		// This is the temporary file created by PHP
		$uploadedfile = $_FILES[$fieldname]['tmp_name'];
		
		$ext = explode(".",$new_file_name);
		$check_ext=$ext[1];
		if(($check_ext=='jpg')||($check_ext=='jpeg')||($check_ext=='JPG')||($check_ext=='JPEG'))
		{
			// Create an Image from it so we can do the resize
			$src = imagecreatefromjpeg($uploadedfile);
		}		
		else if (($check_ext=='gif')||($check_ext=='GIF'))
		{
			$src = imagecreatefromgif($uploadedfile);
		}
		else if (($check_ext=='png')||($check_ext=='PNG'))
		{
			$src = imagecreatefrompng($uploadedfile);
		}
		else if (($check_ext=='bmp')||($check_ext=='BMP'))
		{
			$src = imagecreatefrombmp($uploadedfile);
		}
		// Capture the original size of the uploaded image
		list($width,$height)=getimagesize($uploadedfile);	
		
		$newwidth=$nw;
		$newheight=$nw;
		//$newheight=($height/$width)*$newwidth; 
		$tmp=imagecreatetruecolor($newwidth,$newheight);
		
		$w=$width-$newwidth;
		$h=$height-$newheight;
		// this line actually does the image resizing, copying from the original
		// image into the $tmp image
		$cropedImage = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($cropedImage,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
		
		// now write the resized image to disk. 
		$filename = $path.$new_file_name;
		
		//print"<br>filename=".$filename;
		
		if(($check_ext=='jpg')||($check_ext=='jpeg')||($check_ext=='JPG')||($check_ext=='JPEG'))
		{
			imagejpeg($cropedImage,$filename,100);
		}		
		else if (($check_ext=='gif')||($check_ext=='GIF'))
		{
			imagefilledrectangle($tmp, 0, 0, $newwidth, $newheight, 0xFFFFFF);
			imagecopyresampled($tmp, $src, 0, 0, 0,0, $newwidth, $newheight, $width, $height);		
			imagegif($cropedImage,$filename,100);
		}
		else if (($check_ext=='png')||($check_ext=='PNG'))
		{		
			if(phpversion()>5)
			{
				imagepng($cropedImage,$filename,9,PNG_NO_FILTER);
			}	
			else
			{
				imagefilledrectangle($cropedImage, 0, 0, $newwidth, $newheight, 0xFFFFFF);
				imagecopyresampled($cropedImage, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				imagepng($cropedImage,$filename,100);
			}	
		}
		else if (($check_ext=='bmp')||($check_ext=='BMP'))
		{
			//if(phpversion()>5)
			//{
			//	imagewbmp  (  $tmp  $filename ,255 );
			//}	
			//else
			//{
			//	imagebmp($tmp,$filename,100);
			//}
			imagebmp::imagebmp($img,$filename);
		}
		imagedestroy($src);
		imagedestroy($tmp); // NOTE: PHP will clean up the temp file it created when the request
		// has completed.
	}

	public static function pub($idCat=null,$fixPub,$type)
	{
		if ( $fixPub && $idCat) {
			$query = "SELECT * FROM categories where id_category = $idCat";
		}else {
			$query = "SELECT * FROM categories ORDER BY RAND() limit 1";
		}
		
		$rquery = mysql_query($query);
		$fields = mysql_num_fields($rquery); 
			while ($findlangue1 = mysql_fetch_array($rquery, MYSQL_ASSOC))
			{
				for ($i=0; $i < $fields; $i++  )  //fields
				{ 
					$fieldName = mysql_field_name($rquery, $i); 
					$field_label = "fieldName";
					$$fieldName = $findlangue1[$fieldName];
				}
				
			}
		if ($type == 1 && $pub1) {
			$arrayResult = array('pub1' => $pub1, 'url1' => $url_banner1);
			return $arrayResult;
		}elseif ($type == 2 && $pub2) {
			$arrayResult = array('pub2' => $pub2, 'url2' => $url_banner2);
			return $arrayResult;
		}else {
			$arrayResult = array('pub1' => '', 'url1' => '','pub2' => '', 'url2' => '');
			return $arrayResult;
		}				
	}
	
}//end of myfunction
?>
