<?php 
include("../connection.php");

if(!isset($_SESSION['backoffice_']['user'])){
	header("Location: login.php");
	exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><link rel="SHORTCUT ICON" href="ico.ico"/> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Mauritius report</title>
<meta name="description" content="" /> 
<meta name="keywords" content="">
<meta name="author" content="Freed Graphics">

<link href='http://fonts.googleapis.com/css?family=Droid+Serif|Ubuntu' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo $domain_public; ?>/css/normalize.css">

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo $domain_public; ?>/js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<style type="text/css">
/*margin and padding on body element
  can introduce errors in determining
  element position and are not recommended;
  we turn them off as a foundation for YUI
  CSS treatments. */
body {
	margin:0;
	padding:0;
}

.links a{
	text-decoration:none;
	color:#000000;
}
</style>

</head>
<body>
<script type="text/javascript">

$(function() {
	
});

</script>
<div id="main" style="text-align:center;overflow:hidden;">
	<div class="grid_12" style="text-align:center;"><a href="http://www.mauritiusreport.com"><img src="<?php echo $domain_public; ?>/images/logo.jpg" alt="logo" width="941" height="115" /></a><br /><br /><br /> <hr /><br /><br /></div>
	<!-- Top Part //-->
	<div style="" id="container" >
	    <div class="links" id="links" style="font-size: 14px;float:left;text-align:left;margin-left:20px;margin-bottom:20px;">
		    <?php 
			$query=mysql_query("SELECT * FROM categories");
			$fields = mysql_num_fields($query);
			while ($findlangue1 = mysql_fetch_array($query, MYSQL_ASSOC))
			{
				for ($i=0; $i < $fields; $i++  )  //fields
				{
					$fieldName = mysql_field_name($query, $i);
					$field_label = "fieldName";
					$$fieldName = $findlangue1[$fieldName];
				}
			?>
				<div>
					<a id="manage-pages" href="manage-cat.php?id=<?php echo $id_category; ?>">Manage <?php echo $category; ?></a>
				</div>
				<hr />
			<?php 
			}
			?>
			<div>
				<a id="manage-blog" href="manage-blog.php">Manage Blog</a>
			</div>
			<hr />
			<div>
				<a id="manage-blog" href="manage-guests.php">Manage Guests</a>
			</div>
			<hr />
			<div>
				<a id="manage-comment" href="validate-comment.php">Manage new Comments</a>
			</div>
			<hr />
			<div>
				<a id="manage-documentations" href="manage-documentations.php">Manage documentations</a>
			</div>
			<hr />
			<div>
				<a id="manage-slideshows" href="manage-slideshows.php">Manage slideshows</a>
			</div>
			<hr />
			<br />			
			<div>
				<a href="logout.php">Logout</a>
			</div>
	    
		</div>
	    <br />
	    <div style="float:left;width: 66%;">

